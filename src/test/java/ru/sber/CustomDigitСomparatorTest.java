package ru.sber;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CustomDigitСomparatorTest {

    @Test
    public void compare_Test() {
        CustomDigitСomparator comporator = new CustomDigitСomparator();
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < 11; i++) {
            list.add(i);
        }
        list.add(3);
        list.add(1);
        list.add(7);
        list.add(6);
        list.add(4);
        list.add(8);
        System.out.println(list);
        list.sort(comporator);
        System.out.println(list);
    }

    @Test
    public void compare_Test2() {
        CustomDigitСomparator comporator = new CustomDigitСomparator();
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(1);
        list.add(6);
        list.sort(comporator);
        Assert.assertEquals(Arrays.asList(6, 1, 3), Arrays.asList(list.toArray()));
    }
}