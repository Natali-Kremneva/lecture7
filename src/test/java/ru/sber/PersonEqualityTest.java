package ru.sber;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PersonEqualityTest {

    PersonEquality per1 = new PersonEquality("Georgy", "S.Petersburg", 21);
    PersonEquality per2 = new PersonEquality("IVAN", "S.petersburg", 23);
    PersonEquality per3 = new PersonEquality("Andrey", "Belgorod", 45);
    PersonEquality per4 = new PersonEquality("Aleksei", "PARIS", 39);
    PersonEquality per5 = new PersonEquality("Ivan", "S.PETERSBURG", 23);
    PersonEquality per6 = new PersonEquality("Aleksei", "Paris", 39);

    @Test
    public void getName() {

        List<PersonEquality> persons = new ArrayList<>();
        persons.add(per1);
        persons.add(per2);
        persons.add(per3);
        persons.add(per4);
        persons.add(per5);
        persons.add(per6);

        for (PersonEquality person : persons){
            System.out.println(person.getName());
        }
    }

    @Test
    public void getCity() {
        List<PersonEquality> persons = new ArrayList<>();
        persons.add(per1);
        persons.add(per2);
        persons.add(per3);
        persons.add(per4);
        persons.add(per5);
        persons.add(per6);

        for (PersonEquality person : persons){
            System.out.println(person.getCity());
        }
    }

    @Test
    public void getAge() {
        List<PersonEquality> persons = new ArrayList<>();
        persons.add(per1);
        persons.add(per2);
        persons.add(per3);
        persons.add(per4);
        persons.add(per5);
        persons.add(per6);

        for (PersonEquality person : persons){
            System.out.println(person.getAge());
        }
    }

    @Test
    public void testEquals() {
        List<PersonEquality> persons = new ArrayList<>();
        persons.add(per1);
        persons.add(per2);
        persons.add(per3);
        persons.add(per4);
        persons.add(per5);
        persons.add(per6);

        System.out.println(per2.equals(per5));
        Assert.assertEquals(true, per2.equals(per5));
        Assert.assertEquals(false, per1.equals(per3));
    }

    @Test
    public void testHashCode() {
        List<PersonEquality> persons = new ArrayList<>();
        persons.add(per1);
        persons.add(per2);
        persons.add(per3);
        persons.add(per4);
        persons.add(per5);
        persons.add(per6);

        boolean result = per2.hashCode()==per5.hashCode();
        boolean result2 = per6.hashCode()==per3.hashCode();

        System.out.println(per2.hashCode());
        System.out.println(per5.hashCode());

        Assert.assertEquals(true, result);
        Assert.assertEquals(false, result2);
    }
}