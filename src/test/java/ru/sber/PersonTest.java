package ru.sber;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonTest {

    @Test
    public void compareTo() {
        Person per1 = new Person("Georgy", "S.Petersburg", 21);
        Person per2 = new Person("Ivan", "S.Petersburg", 23);
        Person per3 = new Person("Andrey", "Belgorod", 45);
        Person per4 = new Person("Aleksei", "Paris", 39);

        List<Person> persons = new ArrayList<>();
        persons.add(per1);
        persons.add(per2);
        persons.add(per3);
        persons.add(per4);

        System.out.println(persons);

        Collections.sort(persons,Person::compareTo);
        System.out.println("-----------------------------------------------------");
        System.out.println(persons);

        Person per5 = new Person("Andrey", "Belgorod", 45);

        persons.add(per5);

        Collections.sort(persons,Person::compareTo);
        System.out.println("-----------------------------------------------------");
        System.out.println(persons);

    }
}