package ru.sber;


import junit.framework.TestCase;
import org.junit.Assert;

public class DigitHandlerTest extends TestCase {

    public void testGetValue() {

        DigitHandler digit = new DigitHandler(456);
        Assert.assertEquals(456, digit.getValue());
    }

    public void testTestEquals() {

        DigitHandler digit = new DigitHandler(456);
        boolean res = digit.equals(new DigitHandler(456));
        Assert.assertEquals(true, res);
    }

    public void testTestHashCode() {

        DigitHandler digit1 = new DigitHandler(456);
        DigitHandler digit2 = new DigitHandler(456);
        DigitHandler digit3 = new DigitHandler(546);
        int res = digit1.hashCode();
        int res2 = digit2.hashCode();
        int res3 = digit3.hashCode();
        boolean result = digit1.hashCode() == digit2.hashCode();
        boolean result2 = digit1.hashCode() == digit3.hashCode();
        Assert.assertEquals(true, result);
        Assert.assertEquals(false, result2);
    }
}