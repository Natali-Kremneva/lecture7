package ru.sber;

import java.util.Comparator;

public class CustomDigitСomparator implements Comparator<Integer> {

    @Override
    public int compare(Integer lhs, Integer rhs) {

        if (lhs % 2 == 0 && rhs % 2 == 0 || lhs % 2 != 0 && rhs % 2 != 0){
            if(lhs > rhs) return 1;
            if(lhs < rhs) return -1;
            return 0;
        } else if (lhs % 2 == 0 && rhs % 2 != 0) {
            return -1;
        }
        return 1;
    }
}
