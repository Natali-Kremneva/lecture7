package ru.sber;

import java.util.Objects;

public class DigitHandler {

    private int value;

    public DigitHandler(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "DigitHandler{" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DigitHandler digit = (DigitHandler) obj;

        return value == digit.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
