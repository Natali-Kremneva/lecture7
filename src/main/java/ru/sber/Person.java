package ru.sber;

import java.util.Comparator;

public class Person implements Comparable<Person>{

    private String name;
    private String city;
    private int age;

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    public Person(String name, String city, int age) {
        this.name = name;
        this.city = city;
        this.age = age;
    }

    @Override
    public int compareTo(Person person) {
        if(this.city.equals(person.city)){
            return this.name.compareTo(person.name);
        }
        return this.city.compareTo(person.city);
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}
